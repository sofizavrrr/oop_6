﻿//Напишите программу, которая запрашивает денежную сумму в долларах, а затем выводит эквивалентные суммы в других валютах

#include "pch.h"
#include <iostream>
using namespace std;
int main()
{
	setlocale(0, "");
	const float pound=1.487;
	const float franc = 0.172;
	const float mark = 0.584;
	const float JPY = 0.00955;
	int dollar;

	cout << "Введите количество долларов: ";
	cin >> dollar;
	cout << " 1 фунт = $" << dollar / pound;
	cout << "\n 1 франк = $" << dollar / franc;
	cout << "\n немецкая марка = $" << dollar / mark;
	cout << "\n японская йена = $" << dollar / JPY;
	system("pause");
	return 0;
}

